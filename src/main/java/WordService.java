import java.util.Arrays;
import java.util.HashMap;
import java.util.function.Consumer;
import java.util.regex.Pattern;

public class WordService {

    private String word;

    public WordService(String word) {
        this.word = word;
    }

    public String upperNthSelector(int n) {
        String selected = "";

        if (n <= 0) {
            return selected;
        }

        for (int i = n - 1; i < word.length(); i += n) {
            if (Character.isUpperCase(word.charAt(i))) {
                selected += word.charAt(i);
            }

            if (i + n >= word.length()) {
                return selected;
            }
        }

        return selected;
    }

    //optional 1
    public String upperNumberSpecialNthSelector(int n) {
        String selected = "";

        Pattern regex = Pattern.compile("[^a-z]");

        if (n <= 0) {
            return selected;
        }

        for (int i = n - 1; i < word.length(); i += n) {
            if (regex.matcher(Character.toString(word.charAt(i))).matches()) {
                selected += word.charAt(i);
            }

            if (i + n >= word.length()) {
                return selected;
            }
        }

        return selected;
    }


    //OPTIONAL 2
    public String upperLettersSelectorCounter(int n) {
        String selected = upperNthSelector(n);
        String[] characters = selected.split("");

        HashMap<String, Integer> characterCount = new HashMap<>();

        Arrays.stream(characters).forEach(character -> {
            if (!characterCount.containsKey(character)) {
                characterCount.put(character, 1);
            } else {
                characterCount.put(character, characterCount.get(character) + 1);
            }
        });

        characterCount.forEach((letter, count) -> System.out.println(letter + " = " + count));

        return selected;
    }


}
