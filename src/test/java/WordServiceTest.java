import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;

import static org.junit.Assert.assertEquals;

public class WordServiceTest {

    private WordService wordService;
    private WordService optional1;
    private WordService optional2;


    @Before
    public void setup() {
        wordService = new WordService("ITCLiNicAl");
        optional1 = new WordService("!tCL1Nical");
        optional2 = new WordService("ItCLINiCAL");
    }

    @Test
    public void checkIfUpperNthSelectorReturnsExpectedValue() {
        String result1 = wordService.upperNthSelector(1);
        String expected1 = "ITCLNA";

        String result2 = wordService.upperNthSelector(2);
        String expected2 = "TLN";

        String result3 = wordService.upperNthSelector(3);
        String expected3 = "CNA";

        assertEquals(result1, expected1);
        assertEquals(result2, expected2);
        assertEquals(result3, expected3);
    }

    @Test
    public void checkIfOutBoundsNumberReturnsEmptyString() {
        String result1 = wordService.upperNthSelector(100);
        String expected1 = "";

        String result2 = wordService.upperNthSelector(1000);
        String expected2 = "";

        String result3 = wordService.upperNthSelector(9999999);
        String expected3 = "";

        assertEquals(result1, expected1);
        assertEquals(result2, expected2);
        assertEquals(result3, expected3);
    }

    @Test
    public void checkIfNegativeNumberReturnsEmptyString() {
        String result1 = wordService.upperNthSelector(-1);
        String expected1 = "";

        String result2 = wordService.upperNthSelector(-100);
        String expected2 = "";

        String result3 = wordService.upperNthSelector(-9999999);
        String expected3 = "";

        assertEquals(result1, expected1);
        assertEquals(result2, expected2);
        assertEquals(result3, expected3);
    }

    //OPTIONAL 1

    @Test
    public void checkIfOptionalOneReturnsExpectedValue() {
        String result1 = optional1.upperNumberSpecialNthSelector(1);
        String expected1 = "!CL1N";

        String result2 = optional1.upperNumberSpecialNthSelector(2);
        String expected2 = "LN";

        String result3 = optional1.upperNumberSpecialNthSelector(3);
        String expected3 = "CN";

        assertEquals(result1, expected1);
        assertEquals(result2, expected2);
        assertEquals(result3, expected3);

    }

    //OPTIONAL 2
    @Test
    public void checkIfOptionalTwoReturnsExpectedValue() {
        String result1 = optional2.upperLettersSelectorCounter(1);
        String expected1 = "ICLINCAL";
        assertEquals(result1, expected1);
    }
}
